<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['posts'] = Timber::get_posts();
$context['pagination'] = Timber::get_pagination();

$context['sidebar_menu'] = mcc_get_page_sidebar_menu( get_option( 'page_for_posts' ) );
$context['sidebar_content'] =  mcc_get_page_sidebar_content( get_option( 'page_for_posts' ) );
$context['sidebar_buttons'] =  mcc_get_page_sidebar_buttons( get_option( 'page_for_posts' ) );

if ( ! empty( $context['sidebar_menu'] ) || ! empty( $context['sidebar_content'] ) || ! empty( $context['sidebar_buttons'] ) ) {
	$context['sidebar'] = true;
	$context['body_class'] = implode( ' ', get_body_class('has-sidebar') );
}

Timber::render( 'blog.twig', $context );