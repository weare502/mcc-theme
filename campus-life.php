<?php
/**
 * Template Name: Campus Life
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['jobs'] = Timber::get_posts( array( 'post_type' => 'job', 'posts_per_page' => 6, 
	'tax_query' => array(
		array(
			'taxonomy' => 'job_category',
			'terms' => array( 'internships', 'internship', 'on-campus-jobs', 'campus-jobs' ),
			'field' => 'slug'
		)
	)
) );

$templates = array( 'campus-life.twig' );

Timber::render( $templates, $context );
