<?php
/**
 * Template Name: Alumni
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$templates = array( 'alumni.twig' );
$context['jobs'] = Timber::get_posts( array( 'post_type' => 'job', 'posts_per_page' => 6, 
	'tax_query' => array(
		array(
			'taxonomy' => 'job_category',
			'terms' => array( 'career-opportunities', 'career' ),
			'field' => 'slug'
		)
	)
) );


Timber::render( $templates, $context );
