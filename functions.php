<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

include "inc/MCC-Event.php";

class MCCSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'rest_api_init', array( $this, 'register_api_routes' ) );
		add_action( 'save_post', array( $this, 'update_job_geolocation' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'dashboard_glance_items', array( $this, 'dashboard_glance_items' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
		add_filter( 'wp_nav_menu_objects', array( $this, 'wp_nav_menu_objects' ), 10, 2);
		add_filter( 'tribe_events_get_days_of_week', function( $days_of_week ){
			$days_of_week = Tribe__Events__Main::instance()->daysOfWeekShort;
			$start_of_week = get_option( 'start_of_week', 0 );
			for ( $i = 0; $i < $start_of_week; $i ++ ) {
				$day = $days_of_week[ $i ];
				unset( $days_of_week[ $i ] );
				$days_of_week[ $i ] = $day;
			}
			return $days_of_week;
		}, 10, 1 );
		add_filter( 'facetwp_is_main_query', function( $is_main_query, $query ) {
		    if ( isset( $query->query_vars['facetwp'] ) ) {
		        $is_main_query = $query->query_vars['facetwp'];
		    }
		    return $is_main_query;
		}, 999, 2 );
		add_filter( 'facetwp_assets', function( $assets ) {
		    unset( $assets['gmaps'] );
		    return $assets;
		});

		add_action( 'init', function(){
			add_editor_style( get_stylesheet_uri() );
			include "inc/post-type-job.php";
			include "inc/shortcake.php";
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		$context['breadcrumbs'] = function_exists('yoast_breadcrumb') ? yoast_breadcrumb( '<div id="breadcrumbs">', '</div>', false ) : "";
		
		return $context;
	}

	function after_setup_theme(){
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'secondary', 'Secondary Navigation' );
		register_nav_menu( 'tertiary', 'Tertiary Navigation' );
		register_nav_menu( 'footer', 'Footer Navigation' );

		// register_sidebar( 'sidebar');
		// Images Sizes
		add_image_size( 'xlarge', 2400, 1500 );
	}

	function enqueue_scripts(){
		// Dependencies
		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120206' );
		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'mcc-video', get_template_directory_uri() . "/static/js/container.player.min.js", array( 'jquery', 'underscore' ), '20160820', true );
		wp_enqueue_script( 'mcc-slick', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.js', array( 'jquery' ), '20120206', true );
		// wp_enqueue_script( 'mcc-vue', get_template_directory_uri() . "/static/js/vue.js", array( 'jquery', 'underscore' ), '20160821', true );
		wp_enqueue_style( 'mcc-slick-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css', '20120206' );
		wp_enqueue_style( 'mcc-slick-theme-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick-theme.css', '20120206' );
		wp_localize_script( 'magnific-popup', "wpThemeUrl", get_stylesheet_directory_uri() );
		
		// Main JS
		wp_enqueue_script( 'mcc-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore', 'magnific-popup' ), '20160832' );
	}

	function admin_head_css(){
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
			// array(  
			// 	'title'    => 'Red Text',  
			// 	'selector' => '*',  
			// 	'classes'  => 'red-text',
			// 	// font awesome must be available in the admin area to see the icon
			// 	'icon'     => ' fa fa-eye-dropper'
			// ),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		return $init_array;  
	  
	}

	function register_post_types(){
		// include 'inc/post-type-example.php';
	}

	function dashboard_glance_items( $items ){
		foreach ( get_post_types(array('public'=>true)) as $post_type ){
			$num_posts = wp_count_posts( $post_type );
			if ( $num_posts && $num_posts->publish ) {
				if ( 'post' == $post_type ) {
					continue;
				}
				if ( 'page' == $post_type ) {
					continue;
				}
				$post_type_object = get_post_type_object( $post_type );
				$text = _n( '%s ' . $post_type_object->labels->singular_name, '%s ' . $post_type_object->label, $num_posts->publish );
				$text = sprintf( $text, number_format_i18n( $num_posts->publish ) );
				if ( $post_type_object && current_user_can( $post_type_object->cap->edit_posts ) ) {
					$items[] = sprintf( '<a href="edit.php?post_type=%1$s">%2$s</a>', $post_type, $text );
				} else {
					$items[] = sprintf( '<span>%2$s</span>', $post_type, $text );
				}
			}
		}
		
		return $items;
	}

	function register_api_routes(){
		// register_rest_route() handles more arguments but we are going to stick to the basics for now.
		register_rest_route( 'mcc-theme/v1', '/alert', array(
		    // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
		    'methods'  => WP_REST_Server::READABLE,
		    // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
		    'callback' => array( $this, 'get_alert_info'),
		) );
	}

	/**
	 * This is our callback function that embeds our phrase in a WP_REST_Response
	 */
	function get_alert_info() {
	    // rest_ensure_response() wraps the data we want to return into a WP_REST_Response, and ensures it will be properly returned.
	    return rest_ensure_response( array(
	    	'active' => get_field('alert_active', 'option'),
	    	'title' => get_field('alert_title', 'option'),
	    	'message' => get_field('alert_message', 'option'),
	    	'link' => get_field('alert_link', 'option'),
	    	'type' => get_field('alert_type', 'option')
	    ) );
	}

	function wp_nav_menu_objects( $items, $args ) {
		foreach( $items as &$item ) {
			$icon = get_field('icon', $item);
			// append icon
			if( $icon ) {
				$item->title = '<i class="fa '.$icon.'"></i> ' . $item->title;	
			}

			$new_column = get_field('new_column', $item);
			if ( $new_column ){
				$item->classes[] = "new-column";
			}

			$header = get_field('header', $item);
			if ( $header ){
				$item->classes[] = "sub-menu-header";
			}
		}
		return $items;
	}

	function update_job_geolocation( $post_id ){
		if ( ! isset( $_POST['acf']['field_599ec8f807479'] ) ){
			return;
		}
		$address = $_POST['acf']['field_599ec8f807479'];
		$address_hash = md5( $address );
		$coordinates = get_transient( $address_hash );
		if ( $coordinates === false ) {
			// API key without Referrer Restrictions https://console.developers.google.com/apis/credentials
			$args       = array( 'key' => $api_key, 'address' => urlencode( $address ), 'key' => "AIzaSyDw3OwYSEVcxzxydXLzwWl-DtLGApFH0To" );
			$url        = add_query_arg( $args, 'https://maps.googleapis.com/maps/api/geocode/json' );
			$response 	= wp_remote_get( $url );

			if( is_wp_error( $response ) ) {
				return;
			}

			$data = wp_remote_retrieve_body( $response );

			if( is_wp_error( $data ) ) {
				return;
			}

			if ( $response['response']['code'] == 200 ) {
				$data = json_decode( $data );
				if ( $data->status === 'OK' ) {
					$coordinates = $data->results[0]->geometry->location;
					$cache_value['lat'] 	= $coordinates->lat;
					$cache_value['lng'] 	= $coordinates->lng;
					update_post_meta( $post_id, 'latitude', $coordinates->lat );
					update_post_meta( $post_id, 'longitude', $coordinates->lng );
					$cache_value['address'] = (string) $data->results[0]->formatted_address;
					// cache coordinates for 1 hour
					set_transient($address_hash, $cache_value, 60);
				}
			}
		}

	}

}

new MCCSite();

function mcc_render_primary_menu(){ // used in header.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'primary-menu'
	) );
}

function mcc_render_secondary_menu(){ // used in header.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'secondary',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'secondary-menu'
	) );
}

function mcc_render_tertiary_menu(){ // used in header.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'tertiary',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'tertiary-menu'
	) );
}

function mcc_render_footer_menu(){ // used in header.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'footer',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'footer-menu'
	) );
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site Wide Alerts',
		'menu_title'	=> 'Site Wide Alerts',
		'menu_slug' 	=> 'site-wide-alerts',
		'icon_url'		=> 'dashicons-warning',
		'capability'	=> 'edit_posts',
		'position'		=> 1,
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Footer Options',
		'menu_title'	=> 'Site Footer Options',
		'menu_slug' 	=> 'footer-options',
		'icon_url'		=> 'dashicons-layout',
		'capability'	=> 'edit_posts',
		// 'position'		=> 1,
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Global Site Options',
		'menu_title'	=> 'Global Site Options',
		'menu_slug' 	=> 'global-options',
		'icon_url'		=> 'dashicons-admin-site',
		'capability'	=> 'edit_posts',
		// 'position'		=> 1,
		'redirect'		=> false
	));
	
}

function mcc_get_page_sidebar_menu( $post = null ){
	if ( empty( $post ) ){
		return false;
	}

	if ( is_numeric( $post ) ){
		$post = Timber::get_post($post);
	}
	
	// array of ancestors. The "root" level page will be last
	$parents = get_post_ancestors( $post->ID );

	if ( empty( $parents ) ) {
		// This is a root level page; Grab the sidebar menu.
		$menu = get_field( 'sidebar_menu', $post->ID );
		if ( !empty( $menu ) ){
			return new TimberMenu( $menu );
		}
		return false;
	} else {
		// return the sidebar of the root page.
		$menu = get_field( 'sidebar_menu', $parents[ count($parents) - 1 ] );
		if ( !empty( $menu ) ){
			return new TimberMenu( $menu );
		}
		return false;
	}
}

function mcc_get_page_sidebar_content( $post = null ){
	if ( empty( $post ) ){
		return false;
	}

	if ( is_numeric( $post ) ){
		$post = Timber::get_post($post);
	}
	
	// array of ancestors. The "root" level page will be last
	$parents = get_post_ancestors( $post->ID );

	if ( empty( $parents ) ) {
		// This is a root level page; Grab the sidebar content.
		return get_field( 'sidebar_content', $post->ID );
	} else {
		// return the sidebar of the root page.
		return get_field( 'sidebar_content', $parents[ count($parents) - 1 ] );
	}
}

function mcc_get_page_sidebar_buttons( $post = null ){
	if ( empty( $post ) ){
		return false;
	}

	if ( is_numeric( $post ) ){
		$post = Timber::get_post($post);
	}
	
	// array of ancestors. The "root" level page will be last
	$parents = get_post_ancestors( $post->ID );

	if ( empty( $parents ) ) {
		// This is a root level page; Grab the sidebar buttons.
		return get_field( 'sidebar_buttons', $post->ID );
	} else {
		// return the sidebar of the root page.
		return get_field( 'sidebar_buttons', $parents[ count($parents) - 1 ] );
	}
}

function mcc_get_footer_events() {
	$events = array();
	if ( function_exists( 'tribe_get_events') ){
		$events = Timber::get_posts( tribe_get_events( array( 
			'posts_per_page' => 3,
			'tribeHideRecurrence' => 1,
			'eventDisplay' => 'upcoming',
		) ), "MCCEvent" );
	}
	return $events;
}