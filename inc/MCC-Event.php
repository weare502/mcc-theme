<?php
class MCCEvent extends TimberPost {
	public function event_start_day(){
		$timestamp = strtotime($this->EventStartDate);
		return date("d", $timestamp);
	}

	public function event_start_month(){
		$timestamp = strtotime($this->EventStartDate);
		return date("M", $timestamp);
	}

	public function event_start_time(){
		$timestamp = strtotime($this->EventStartDate);
		return date("g:i A", $timestamp);
	}

	public function event_end_time(){
		$timestamp = strtotime($this->EventEndDate);
		return date("g:i A", $timestamp);
	}
}