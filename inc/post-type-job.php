<?php
// Job Post Type
$labels = array(
	'name'                => __( 'Jobs', 'starter' ),
	'singular_name'       => __( 'Job', 'starter' ),
	'add_new'             => _x( 'Add New Job', 'starter', 'starter' ),
	'add_new_item'        => __( 'Add New Job', 'starter' ),
	'edit_item'           => __( 'Edit Job', 'starter' ),
	'new_item'            => __( 'New Job', 'starter' ),
	'view_item'           => __( 'View Job', 'starter' ),
	'search_items'        => __( 'Search Jobs', 'starter' ),
	'not_found'           => __( 'No Jobs found', 'starter' ),
	'not_found_in_trash'  => __( 'No Jobs found in Trash', 'starter' ),
	'parent_item_colon'   => __( 'Parent Job:', 'starter' ),
	'menu_name'           => __( 'Jobs & Internships&emsp;', 'starter' ),
);

$args = array(
	'labels'                   => $labels,
	'hierarchical'        => false,
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-paperclip',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor' )
);

register_post_type( 'job', $args );

// Job Category
$labels = array(
	'name'                  => _x( 'Job Categories', 'Taxonomy Job Categories', 'text-domain' ),
	'singular_name'         => _x( 'Job Category', 'Taxonomy Job Category', 'text-domain' ),
	'search_items'          => __( 'Search Job Categories', 'text-domain' ),
	'popular_items'         => __( 'Popular Job Categories', 'text-domain' ),
	'all_items'             => __( 'All Job Categories', 'text-domain' ),
	'parent_item'           => __( 'Parent Job Category', 'text-domain' ),
	'parent_item_colon'     => __( 'Parent Job Category', 'text-domain' ),
	'edit_item'             => __( 'Edit Job Category', 'text-domain' ),
	'update_item'           => __( 'Update Job Category', 'text-domain' ),
	'add_new_item'          => __( 'Add New Job Category', 'text-domain' ),
	'new_item_name'         => __( 'New Job Category Name', 'text-domain' ),
	'add_or_remove_items'   => __( 'Add or remove Job Categories', 'text-domain' ),
	'choose_from_most_used' => __( 'Choose from most used Job Categories', 'text-domain' ),
	'menu_name'             => __( 'Job Category', 'text-domain' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => false,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => false,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'job_category', array( 'job' ), $args );

// Job State
$labels = array(
	'name'                  => _x( 'States', 'Taxonomy States', 'text-domain' ),
	'singular_name'         => _x( 'State', 'Taxonomy State', 'text-domain' ),
	'search_items'          => __( 'Search States', 'text-domain' ),
	'popular_items'         => __( 'Popular States', 'text-domain' ),
	'all_items'             => __( 'All States', 'text-domain' ),
	'parent_item'           => __( 'Parent State', 'text-domain' ),
	'parent_item_colon'     => __( 'Parent State', 'text-domain' ),
	'edit_item'             => __( 'Edit State', 'text-domain' ),
	'update_item'           => __( 'Update State', 'text-domain' ),
	'add_new_item'          => __( 'Add New State', 'text-domain' ),
	'new_item_name'         => __( 'New State Name', 'text-domain' ),
	'add_or_remove_items'   => __( 'Add or remove States', 'text-domain' ),
	'choose_from_most_used' => __( 'Choose from most used States', 'text-domain' ),
	'menu_name'             => __( 'State', 'text-domain' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => false,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => false,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'job_state', array( 'job' ), $args );


// Job Country
$labels = array(
	'name'                  => _x( 'Countries', 'Taxonomy Countries', 'text-domain' ),
	'singular_name'         => _x( 'Country', 'Taxonomy Country', 'text-domain' ),
	'search_items'          => __( 'Search Countries', 'text-domain' ),
	'popular_items'         => __( 'Popular Countries', 'text-domain' ),
	'all_items'             => __( 'All Countries', 'text-domain' ),
	'parent_item'           => __( 'Parent Country', 'text-domain' ),
	'parent_item_colon'     => __( 'Parent Country', 'text-domain' ),
	'edit_item'             => __( 'Edit Country', 'text-domain' ),
	'update_item'           => __( 'Update Country', 'text-domain' ),
	'add_new_item'          => __( 'Add New Country', 'text-domain' ),
	'new_item_name'         => __( 'New Country Name', 'text-domain' ),
	'add_or_remove_items'   => __( 'Add or remove Countries', 'text-domain' ),
	'choose_from_most_used' => __( 'Choose from most used Countries', 'text-domain' ),
	'menu_name'             => __( 'Country', 'text-domain' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => false,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => false,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'job_country', array( 'job' ), $args );