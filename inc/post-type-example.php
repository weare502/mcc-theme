<?php
$labels = array(
	'name'                => __( 'examples', 'starter' ),
	'singular_name'       => __( 'example', 'starter' ),
	'add_new'             => _x( 'Add New example', 'starter', 'starter' ),
	'add_new_item'        => __( 'Add New example', 'starter' ),
	'edit_item'           => __( 'Edit example', 'starter' ),
	'new_item'            => __( 'New example', 'starter' ),
	'view_item'           => __( 'View example', 'starter' ),
	'search_items'        => __( 'Search examples', 'starter' ),
	'not_found'           => __( 'No examples found', 'starter' ),
	'not_found_in_trash'  => __( 'No examples found in Trash', 'starter' ),
	'parent_item_colon'   => __( 'Parent example:', 'starter' ),
	'menu_name'           => __( 'examples', 'starter' ),
);

$args = array(
	'labels'                   => $labels,
	'hierarchical'        => false,
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-hammer',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor', 'thumbnail',
		)
);

register_post_type( 'example', $args );
