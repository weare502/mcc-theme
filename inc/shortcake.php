<?php

add_shortcode('jobs-embed', function($attr){
    $attr = wp_parse_args( $attr, array(
        'display' => 'simple',
        'category' => '',
    ) );
    $cats = array_map( 'trim', explode( ',', $attr['category'] ) );
    $cats = array_map( 'intval', $cats );
    $tax_query = array(
        array(
            'taxonomy' => 'job_category',
            'terms' => $cats
        )
    );
    if ( $cats[0] === 0 ){
        $tax_query = null;
    }

    // $context = Timber::get_context();
    $context['embed_type'] = $attr['display'];
    $query = new WP_Query( array( 
        'post_type' => 'job', 
        'tax_query' => $tax_query,
        'posts_per_page' => 250,
        'facetwp' => true,
    ) );
    $context['jobs'] = Timber::query_posts( $query );
    $ret = Timber::compile( 'jobs-embed.twig', $context );
    
    return $ret;
} );

if ( ! function_exists( 'shortcode_ui_register_for_shortcode' ) ) {
    return; // bail early if shortcake isn't installed
}

shortcode_ui_register_for_shortcode(
    'jobs-embed', array(
        'label' => 'Jobs Embed',
        'listItemImage' => 'dashicons-hammer',
        'attrs' => array(
            array(
                'label' => 'Job Category',
                'attr' => 'category',
                'type' => 'term_select',
                'taxonomy' => 'job_category',
                'multiple' => true,
                'description' => 'Start typing to select multiple categories or leave blank to show all jobs. Autocomplete may be slow depending on your internet connection speed.',
            ),
            array(
                'label' => 'Display Type',
                'description' => 'Choose a display type. Simple is a boxed list while complex is a full table embed with a map.',
                'attr' => 'display',
                'type' => 'select',
                'options' => array(
                    array(
                        'value' => 'simple',
                        'label' => 'Simple'
                    ),
                    array(
                        'value' => 'complex',
                        'label' => 'Complex'
                    ),
                ),
            )
        )
    )
);
