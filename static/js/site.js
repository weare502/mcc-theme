/* jshint devel: true */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position){
			position = position || 0;
			return this.substr(position, searchString.length) === searchString;
	};
}

function templatePolyfill() {
  if ('content' in document.createElement('template')) {
    return false;
  }

  var templates = document.getElementsByTagName('template');
  var plateLen = templates.length;

  for (var x = 0; x < plateLen; ++x) {
    var template = templates[x];
    var content = template.childNodes;
    var fragment = document.createDocumentFragment();

    while (content[0]) {
      fragment.appendChild(content[0]);
    }

    template.content = fragment;
  }
}

templatePolyfill();

/**
 * FitVids JS
 */
;(function( $ ){

	'use strict';

	$.fn.fitVids = function( options ) {
		var settings = {
			customSelector: null,
			ignore: null
		};

		if(!document.getElementById('fit-vids-style')) {
			// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
			var head = document.head || document.getElementsByTagName('head')[0];
			var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
			var div = document.createElement("div");
			div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
			head.appendChild(div.childNodes[1]);
		}

		if ( options ) {
			$.extend( settings, options );
		}

		return this.each(function(){
			var selectors = [
				'iframe[src*="player.vimeo.com"]',
				'iframe[src*="youtube.com"]',
				'iframe[src*="youtube-nocookie.com"]',
				'iframe[src*="kickstarter.com"][src*="video.html"]',
				'object',
				'embed'
			];

			if (settings.customSelector) {
				selectors.push(settings.customSelector);
			}

			var ignoreList = '.fitvidsignore';

			if(settings.ignore) {
				ignoreList = ignoreList + ', ' + settings.ignore;
			}

			var $allVideos = $(this).find(selectors.join(','));
			$allVideos = $allVideos.not('object object'); // SwfObj conflict patch
			$allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

			$allVideos.each(function(){
				var $this = $(this);
				if($this.parents(ignoreList).length > 0) {
					return; // Disable FitVids on this video.
				}
				if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
				if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
				{
					$this.attr('height', 9);
					$this.attr('width', 16);
				}
				var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
						width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
						aspectRatio = height / width;
				if(!$this.attr('name')){
					var videoName = 'fitvid' + $.fn.fitVids._count;
					$this.attr('name', videoName);
					$.fn.fitVids._count++;
				}
				$this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
				$this.removeAttr('height').removeAttr('width');
			});
		});
	};
	
	// Internal counter for unique video names.
	$.fn.fitVids._count = 0;
	
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */

jQuery( document ).ready( function( $ ) {
	var $body = $('body');
	$body.fitVids();

	$('.open-popup-link').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});
	
	$('.magnific-trigger').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true
	});

	$(function() {
		if ( ! window.HomeVideoID ) {
			return;
		}

		$('body.home').ContainerPlayer({
			overlay: { color: 'rgba(0,0,0,0.4)' },
			vimeo: {  
				tranitionIn: true,
				videoId: window.HomeVideoID,
				poster: window.HomeVideoPoster,
			},
			/*youTube: {  
				videoId: 'quG1iBo8Gc8',
				poster: '../test/background2.jpg',
			},*/
		}).on('video.playing video.paused video.loaded video.ended', function(e) {
			console.log(e);
		}).on('video.loaded', function(){
			$('body').addClass('playing-video');
		});
	});

	$('#header #searchform > div').addClass('search-fields-wrapper');
	$('#searchform #s').attr('placeholder', 'Search');

	$('#menu-toggle').on('click', function(){
		$('html').toggleClass('mobile-menu-open');
	});

	$('#primary-menu > .menu-item-has-children').find('.sub-menu').wrap('<div class="mega-menu-wrapper"></div>');
	$('#primary-menu .mega-menu-wrapper .sub-menu > li:first-child').addClass('new-column').unwrap();
	$("#primary-menu .mega-menu-wrapper li").each(function () {
		if ( $(this).hasClass('new-column') ){
		    $(this).nextUntil(".new-column").addBack().wrapAll("<ul class='sub-menu'></ul>");
		}
	});
	$('#header .nav-wrapper').addClass('loaded');

	  
	$( '#primary-menu' ).on( 'mouseenter focus', '.menu-item-has-children > a', function(  ) {
			var el = $( this );
	    el.toggleClass( 'has-focus' );
			// Show sub-menu
			el.parents( '.menu-item' ).attr( 'aria-expanded', 'true' );
		}).on( 'mouseleave blur', '.menu-item-has-children > a', function( ) {
			var el = $( this );
	    el.toggleClass( 'has-focus' );
			// Only hide sub-menu after a short delay, so links get a chance to catch focus from tabbing
			setTimeout( function() {
				if ( el.siblings( '.sub-menu' ).attr( 'data-has-focus' ) !== 'true' ) {
					el.parents( '.menu-item' ).attr( 'aria-expanded', 'false' );
				}
			}, 100 );
		}).on( 'mouseenter focusin', '.mega-menu-wrapper', function(  ) {
			var el = $( this );
			el.attr( 'data-has-focus', 'true' );
		}).on( 'mouseleave focusout', '.mega-menu-wrapper', function(  ) {
			var el = $( this );
			setTimeout( function() {
				// Check if anything else has picked up focus (i.e. next link in sub-menu)
				if ( el.find( ':focus' ).length === 0 ) {
					el.attr( 'data-has-focus', 'false' );						
					// Hide sub-menu on the way out if parent link doesn't have focus now
        	if ( el.siblings( 'a.has-focus' ).length === 0 ) {
				el.parents( '.menu-item-has-children' ).attr( 'aria-expanded', 'false' );
          }
        }
			}, 100 );
		});

	$('.mobile-menu #primary-menu > li').append('<button class="sub-menu-trigger"><span class="fa fa-chevron-right"></span></button>');
	$('.mobile-menu .sub-menu-trigger').click(function(){
		$(this).parent().toggleClass('show-mega-menu');
	});

	$.ajax( {
		url: '/wp-json/mcc-theme/v1/alert',
		method: 'GET'
	} ).done( function( data ){
		if ( ! data.active ){
			return;
		}
		
		var $template = jQuery('#alert-template');
		var $message = $template.find('.alert-message'),
			$title = $template.find('.alert-title'),
			$icon = $template.find('.alert-icon'),
			$container = $template.find('.site-alert');

		$message.html( data.message );
		$title.html( data.title + ":" );
		$container.addClass( data.type.toLowerCase() );
		$icon.addClass( data.type.toLowerCase() );

		if ( data.link ){
			$container.wrap(function(){
				return "<a class='site-alert-link' href='" + data.link + "'></a>";
			});
		}

		$('body').prepend( $template.html() );
	} );

	$('body.home .home-image-slider').slick({
		arrows: true,
		autoplay: true,
		autoplaySpeed: 5000,
	});

	$('body.home .video-spotlight .container').slick({
		arrows: true
	});

	$('.admissions-featured-content .feature-gallery').slick({
		dots: true,
		adaptiveHeight: true
	});

	$('.expand-sub-menu').click('on', function(){
		$(this).siblings('.sub-menu').toggleClass('show');
		$(this).toggleClass('dropdown-open');
	});

	// var $jobs = $('.employment-internships-list li');
	// if ( $jobs.length > 6 ){
	// 	$($jobs.slice(6)).hide();
	// 	$('#show-more-jobs').click(function(){
	// 		$jobs.show();
	// 		$(this).hide();
	// 	});
	// } else {
	// 	$('#show-more-jobs').hide();
	// }

	$('.mcc-event-wrapper .tribe-mini-calendar-list-wrapper').append("<p class='centered-text'><a href='/events' class='button gold'>View All Events</a></p>");

	// $('.wrap-embed-contact-form .btn-show-contact').inner( $('.quick-connect a') );
}); // End Document Ready

function replacePopoutButton(){
	jQuery( document ).ajaxComplete(function() {
		if ( window.modifiedEmbedButtom === true ){
			return;
		}
		if ( jQuery('.wrap-embed-contact-form .embed-contact-form .btn-show-contact').length > 0 ){
			jQuery('.wrap-embed-contact-form .embed-contact-form .btn-show-contact').html("Need Help? <br><span>Connect with Us</span>");
			window.modifiedEmbedButtom = true;
		}
	});
}

jQuery(document).ready(function($){
	// https://stackoverflow.com/questions/15719951/google-maps-api-v3-auto-center-map-with-multiple-markers
	if ( ! document.getElementById('jobs-embed-map') ){
		// bail if we don't have a jobs map
		return;
	}
	$(window).load(function(){
		var map = new google.maps.Map(document.getElementById('jobs-embed-map'), {
			zoom: 3,
			center: {lat: 42.709191766596696, lng: -74.5188756287098}
		});
		var geocoder = new google.maps.Geocoder();
		var bounds = new google.maps.LatLngBounds();
		var infowindow = new google.maps.InfoWindow();
		var markers = new Array();
		$items = $('.jobs-embed-complex .job-row');
		$items.each(function(i){
			var $this = $(this);
			var city = $this.find('.job-city').text();
			var state = $this.find('.job-state').text();
			var country = $this.find('.job-country').text();
			var markerContent = $this.find('.job-title').html();
			var lat = parseFloat($this.attr('data-lat'));
			var lng = parseFloat($this.attr('data-lng'));
			var address = '';
			if ( city && state && country ){
				address = city + ", " + state + ", " + country;
				altAddress = city + ", " + state;
				addAddressToMap(address, geocoder, map, bounds, infowindow, markerContent, altAddress, lat, lng);
			} 
		});
		// var listener = google.maps.event.addListener(map, "idle", function () {
		//     map.setZoom(3);
		//     google.maps.event.removeListener(listener);
		// });
		window.jobsEmbedMap = map;
		window.jobsEmbedTable = $('.jobs-embed-complex table').DataTable({
			'searching': false,
			'lengthChange': false,
			'pageLength': 15,
			'responsive': true,
			// 'scrollX': true,
			'pagingType': 'simple',
			'order': [[0, "desc"]]
			// 'select': 'single'
		});
	});
	
	function addAddressToMap(address, geocoder, resultsMap, bounds, infowindow, markerContent, altAddress, lat, lng) {
		if ( ! lat || ! lng ) return;
		var marker = new google.maps.Marker({
			map: resultsMap,
			position: { lat: lat, lng: lng }
		});
		bounds.extend(marker.position);
		resultsMap.fitBounds(bounds);
		google.maps.event.addListener(marker, 'click', (function(marker) {
			return function() {
				infowindow.setContent(markerContent);
				infowindow.open(resultsMap, marker);
		    }
		})(marker));
		// geocoder.geocode({'address': address}, function(results, status) {
		// 	if (status === 'OK') {
		// 		var marker = new google.maps.Marker({
		// 			map: resultsMap,
		// 			position: results[0].geometry.location
		// 		});
		// 		bounds.extend(marker.position);
		// 		resultsMap.fitBounds(bounds);
		// 		google.maps.event.addListener(marker, 'click', (function(marker) {
		// 			return function() {
		// 				infowindow.setContent(markerContent);
		// 				infowindow.open(resultsMap, marker);
		// 		    }
		// 		})(marker));
		// 		return;
		// 	} else {
		// 		console.log(status);
		// 		console.log("Couldn't find location", address);
		// 		console.log("Trying alt address", altAddress);
		// 		if ( address !== altAddress ){
		// 			// Try again but with the alternate address
		// 			addAddressToMap(altAddress, geocoder, resultsMap, bounds, infowindow, markerContent, altAddress);
		// 		}
		// 		return;
		// 	}
		// });
	}
});

var initMap = window.initMap || (function(){});