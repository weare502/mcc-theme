<?php
/**
 * Template Name: Contact
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

Timber::render( array( 'contact.twig', 'page.twig' ), $context );