<?php
/**
 * Single Event Template for Widgets
 *
 * This template is used to render single events for both the calendar and advanced
 * list widgets, facilitating a common appearance for each as standard.
 *
 * You can override this template in your own theme by creating a file at
 * [your-theme]/tribe-events/pro/widgets/modules/single-event.php
 *
 * @version 4.4.3
 *
 * @package TribeEventsCalendarPro
 */

$mini_cal_event_atts = tribe_events_get_widget_event_atts();

$post_date = tribe_events_get_widget_event_post_date();
$post_id   = get_the_ID();
$tpost = Timber::get_post();

$organizer_ids = tribe_get_organizer_ids();
$multiple_organizers = count( $organizer_ids ) > 1;

$city_name   = ! empty( $city ) ? tribe_get_city() : '';
$region_name = ! empty( $region ) ? tribe_get_region() : '';
$zip_text    = ! empty( $zip ) ? tribe_get_zip() : '';

$has_address_details = ! empty( $city_name ) || ! empty( $region_name ) || ! empty( $zip_text );
?>

<div class="tribe-mini-calendar-event event-<?php esc_attr_e( $mini_cal_event_atts['current_post'] ); ?> <?php esc_attr_e( $mini_cal_event_atts['class'] ); ?>">

	<div class="list-date">

			<span class="list-dayname">
				<?php
				echo apply_filters(
					'tribe-mini_helper_tribe_events_ajax_list_dayname',
					date_i18n( 'M', $post_date ),
					$post_date,
					$mini_cal_event_atts['class']
				);
				?>
			</span>


		<span class="list-daynumber"><?php echo apply_filters( 'tribe-mini_helper_tribe_events_ajax_list_daynumber',
			date_i18n( 'd', $post_date ), $post_date, $mini_cal_event_atts['class'] ); ?></span>
		</div>

		<div class="list-info">
			<?php do_action( 'tribe_events_list_widget_before_the_event_title' ); ?>
			<h2 class="tribe-events-title">
					<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h2>
			<?php do_action( 'tribe_events_list_widget_after_the_event_title' ); ?>

			<?php do_action( 'tribe_events_list_widget_before_the_meta' ) ?>

			<div class="tribe-events-duration">
				<?php echo tribe_events_event_schedule_details(); ?>
			</div>

			<div class="event-excerpt">
				<?php Timber::render( 'event-excerpt.twig', array('post' => $tpost ) ); ?>
			</div>
			
		</div>
	<?php do_action( 'tribe_events_list_widget_after_the_meta' ) ?>
</div> <!-- .list-info -->
