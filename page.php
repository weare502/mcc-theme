 <?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

$context['sidebar_menu'] =  mcc_get_page_sidebar_menu( $post );
$context['sidebar_content'] =  mcc_get_page_sidebar_content( $post );
$context['sidebar_buttons'] =  mcc_get_page_sidebar_buttons( $post );

if ( ! empty( $context['sidebar_menu'] ) || ! empty( $context['sidebar_content'] ) || ! empty( $context['sidebar_buttons'] ) ) {
	$context['sidebar'] = true;
	$context['body_class'] = implode( ' ', get_body_class('has-sidebar') );
}

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );
}

