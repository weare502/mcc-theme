 <?php
/**
 * Template Name: Nested Child Landing Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['child_posts'] = Timber::get_posts( array( 'post_parent' => $post->ID, 'post_type' => 'page', 'posts_per_page' => -1 ) );

$context['sidebar_menu'] =  mcc_get_page_sidebar_menu( $post );
$context['sidebar_content'] =  mcc_get_page_sidebar_content( $post );
$context['sidebar_buttons'] =  mcc_get_page_sidebar_buttons( $post );

if ( ! empty( $context['sidebar_menu'] ) || ! empty( $context['sidebar_content'] ) || ! empty( $context['sidebar_buttons'] ) ) {
	$context['sidebar'] = true;
	$context['body_class'] = implode( ' ', get_body_class('has-sidebar') );
}

Timber::render( array( 'nested-landing-page.twig' ), $context );

