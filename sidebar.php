<?php
/**
 * The Template for displaying the sidebar.
 *
 * @package  WordPress
 * @subpackage  Timber
 */

Timber::render( array( 'sidebar.twig' ), $data );
